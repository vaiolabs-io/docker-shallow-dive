
---

# 08 Managing Images

---

# Managing Images

Essentially, when ever you wish to do something with your image: copy, save or export,  you would prefer to do so with help of docker itself.
That is why you can do all the manament :
- Manually: by export and importing images 
- Automatically: by exporting images to docker hub, or some type of registry for your use case

---

# Saving And Loading Images

Different companies provide different specifications of working closed environments. In typical closed environment, there are no access to docker hub, thus question is asked : how can we save and move image around ? 
The answer is : moving/saving docker images with automation or manually.

---

# Saving And Loading Images (cont.)

That is why we have `save` option with `docker image` subcommand.

To save existing image or image from docker hub

```sh
docker image save my-image > my-saved-image.tar
```
or
```sh
docker image save my-image -o my-saved-image.tar
```
or
```sh
docker image save my-image --output my-saved-image.tar
```
*tar* file will be generated and you'll be able to zip it in any manner you'd wish.
> `[!]` Note: to see what you have inside tar file use **tar tvf my-saved-image.tar**

---

# Saving And Loading Images (cont.)
 
If you'd like to revert to the use of the image, then *load*  command should be used.

Just as with *save*, locate the tar file and *load* back

```sh
docker image load my-image < my-saved-image.tar
```
or
```sh
docker image load my-image -i my-saved-image.tar
```
or
```sh
docker image load my-image --input my-saved-image.tar
```
---

# Practice

- List all custom images that we have created
- Take every each of the images and save them into separate tar files
- Delete all containers and images on the host
- Load the images from tar files

---

# Practice (cont.)

```sh
docker image ls
docker image save py-web-server > py-web-server.tar
docker container stop $(docker ps -a -q)
docker container rm $(docker ps -a -q)
docker image rm $(docker image ls  -q)
docker image load py-web-server < py-web-server.tar
docker image ls # should see only py-web-server
```

---


## Tagging

We already discussed the tagging, but haven't mentioned it in case of `image` command

```sh
 docker image tag my-img/my-app:v1 vaiolabs.io/html-app:latest
```

Why to use it if you created label and tag when generating the image ? 

Well, its general purpose is used when distributing the docker image to/from dockerhub which is our next topic.

---

# Distribution Images On Docker Hub

In case you have not created account at _[docker hub](https://hub.docker.com)_, then __this is the time to do so__.

If  you already have the account then, you can issue  a `docker login` command which will allow to connect to your public and private repositories of images.

Once you have created the account and logged in from your terminal, all is left to `push` your custom image to the docker hub or any other hub there is.

That can be done with 

```sh
docker tag html-app:latest vaiolabs.io/html-app:latest
docker push vaiolabs.io/html-app:latest
```

---

# Practice

- Create account on docker hub at hub.docker.com
- Use `docker login` to connect to docker hub account on your host
- Tag the image you would like to upload, for example py-web-server
- Push the image to docker hub
- Validate that the image exists

---

# Practice (cont.)

```sh
docker login
docker tag py-web-server:latest vaiolabs.io/py-web-server:latest # vaiolabs.io is my account name, yours should be different
docker push vaiolabs.io/py-web-server:latest
```
---

## Image History

After images are created, it is requested to look on build history. we can sort internals of the image with *history* command

Usually providing the image name is enough

```sh
docker image history my-image
```

It should provide you with *image ids* and with description of what ran on each of the layers.
In some cases, the whole image id is required and then we can use *--no-trunc* which will print the whole id of the image
In other cases the shorter image id can be achieved with *--quiet* flag

---

# Practice

- Pull `ubuntu/apache2` image from docker hub
- Run docker history to see what happened while build ing that image
- Use docker history on py-web-server to see that changes we have created

---

# Practice (cont.)

```sh
docker pull ubuntu/apache2
docker history  ubuntu/apache2
docker history py-web-server
```
