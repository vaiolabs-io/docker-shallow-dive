

---

# 09 Docker Compose

<img src="../99_misc/.img/docker-compose-logo.png" alt="drawing" style="border-radius:25px;float:right;width:180px;">
<!-- ![bg right:40% fit](../99_misc/.img/docker-compose-logo.png) -->


Compose is a tool for defining and running multi-container Docker applications. With compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration

---

# Docker Compose (cont.)

Using docker-compose is basically a three-step process:

- Define your app’s libraries, environment, variables and so on, with a `Dockerfile` so it can be reproduced anywhere.
    - It is mostly used for development purposes, as for staging or production we use already built and test container image located container registry.
- Define the services that make up your application in `docker-compose.yml` so they can be run together in an isolated environment.
- Run `docker compose up -d` and the docker-compose command runs configuration as defined in `docker-compose.yml`. 
    - You can alternatively run  `docker-compose up using the docker-compose binary.

---

# Docker Compose (cont.)


Compose has commands for managing the whole lifecycle of your application:

- Start, stop, and rebuild services
- View the status of running services
- Stream the log output of running services
- Run a one-off command on a service


---

#  Installing Docker Compose


Docker Compose relies on *Docker Engine* for any meaningful work, so make sure you have *Docker Engine* installed either locally or remote, depending on your setup.

Since __docker version 22.0.1__ _docker compose_ is included as a plugin in the installation, thus the chances are that our lab has `docker-compose` already installed. However, when it comes major companies they do not install the latest and the greatest, thus the  next segment is for docker version 21.0.0 and below.

---

#  Installing Docker Compose (cont.)

- On desktop systems like Docker Desktop for Mac and Windows, Docker Compose is included as part of those desktop installs.
- On Linux systems, first install the Docker Engine for your OS as described earlier in the course, then run the next command to install *docker-compose*

```sh
 sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)"\
                 -o /usr/local/bin/docker-compose
 sudo chmod +x /usr/local/bin/docker-compose
 sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

The last step is optional, in case you don't have */usr/local/bin* in your *$PATH* variable.
> `[!]` Note: version of docker-compose might be different, depending on when you are reading this, yet this version still should be able for download

---

#  Practice (optional)

- Install docker compose from cli using latest repository at github

```sh
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)"\
                 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose version
```

---

#  Compose Commands

Here is a short list of mainly used commands with *docker-compose*

- build: helps to build the image
- up: creates and starts the containers
- down: stop and remove containers, networks, images and volumes
- ps: list containers
- stop: stop the service
- start: starts the service
- restart: restarts the service

---

#  Creating a Compose File

typically, docker compose file is called **docker-compose.yaml**. one can give a different name , but in that case you'll have use *-f* flag to provide the path for the file to work.

compose files are usually yaml files, although there is an option of writing and using it in json.

the classic docker-compose file usually consists of 4 main top level keys:

- __version__: a mandatory key, that defines to which api of docker you need to communicate with.
- __services__: defines the list of containers that we're going to use. name of the service will the name of the actual container
    - It is possible to do image builds with docker compose  by using *build* key  and pass to it your *context* of actual dockerfile and *args* for build
- __volumes__ :
- __networks__ :

> [!] Notes: 
>   - There additional tags such as `configs` and `secrets`
>   - We'll cover these if time permits

---


# Docker Compose YAML

Docker-Compose was designed with ease in mind, thus YAML configuration and serialization language is used to configure the `docker-compose.yml` file. Due to compatibility to programming languages it is suggested to use `.yml` extension, but in case of use with Linux distros and MacOS, extensions do not matter, thus either `.yaml` or `.yml` will be sufficient.
In case you are working with Windows based system, `.yml` extension needs to be set up before using the with docker-compose.

lets begin with simple example:

```yaml
version: "2"  # version for docker engine api for docker-compose to use
services: # describes containers that will run and associate them as services
  web:   # name of the service: can be anything you like
    image: ubuntu/apache2 # image used for service
    ports: # local and internal ports associated with host and container
      - "8000:80"

```

> [!] Note: api `version: 1` is deprecated please do not use it, unless you know what you are doing.

---

# Practice

- Create docker-compose.yml file
- Set api version to 3
- Set docker-compose services named web and redis
- For web service use nginx image
- For Redis service use redis image
- On web service use port 8000 open on you host and 80 on you container
- On redis service use port 6379 on your host and same on your container
- Run docker-compose to test the file

```sh
touch docker-compose.yml
```

---
# Practice (cont.)


```yaml
version: "2"
services: 
  web:  
    image: nginx
    ports: 
      - "8000:80"
  db: 
    image: redis 
    ports:
      - "6379:6379"
```

```sh
docker-compose -d up # use down to kill previous containers
```
---
# Environment variables in Compose

It is possible to use environment variables in your shell to populate values inside a Compose file:

```sh
TAG="1.1" #declaring environment variable in your shell
```

```yaml
web:
  image: "webapp:${TAG}"
```

---

# Env variables in Compose(cont.)
In case  there is a long list of environment variables, you can set default values for any environment variables referenced in an environment file named `.env`. The `.env` file path is as follows:
- The `.env` file is placed at the base of the project directory.
- Project directory can be explicitly defined with the --file option or `COMPOSE_FILE` environment variable. Otherwise, it is the current working directory where the docker compose command is executed (+1.28).

```sh
cat .env
VERSION="3.7"

cat docker-compose.yml
version: "2"
services:
  app:
    image: "python:${VERSION}"
```

---

# Practice

- Copy docker compose file from previous task and change ports with variables
- Create `.env` file for those same variables
- Run docker-compose with env-file
```yaml
version: "2"
services: 
  web:  
    image: ${WEB-IMAGE}
    ports: 
      - "${WEB-PORT}:80"
  db: 
    image: ${DB-IMAGE}
    ports:
      - "${DB-PORT}:${DB-PORT}"
```
```sh
cat .env
WEB-IMAGE="nginx"
WEB-PORT=8000
DB-IMAGE="redis"
DB-PORT=6379

docker-compose  -d --env-file .env up # path to .env file may vary
```

---

# Setting Environment Variables To Containers

We can set environment variables in a service’s containers with the `environment` key, just like with docker run -e VARIABLE=VALUE ...:
```yaml
web:
  environment:
    - DEBUG=1
```
We can pass multiple environment variables from an external file through to a service’s containers with the `env_file` option, just like with docker run --env-file=FILE ...:
```yaml
web:
  env_file:
    - web-variables.env
```
---

# Using Network Docker Compose

## Overview
Lets start by overview of what compose provides us from network point of view:


- The Default where all containers visible to each other  <!-- ![bg right:40% width:100px](../99_misc/.img/compose_net_1.png) -->
 <img src="../99_misc/.img/compose_net_1.png" alt="compose net 1" style="border-radius:25px;float:right;width:200px;">
- Containers visible only within networks    <!-- ![bg right:40% width:300px](../99_misc/.img/compose_net_2.png) -->
<img src="../99_misc/.img/compose_net_2.png" alt="compose net 2" style="border-radius:25px;float:right;width:300px;">
- Container may join multiple networks  <!-- ![bg right:40% width:300px](../99_misc/.img/compose_net_3.png) -->
 <img src="../99_misc/.img/compose_net_3.png" alt="compose net 3" style="border-radius:25px;float:right;width:300px;">


---

# Default network

Like `docker network` command, `docker-compose` uses access to `docker-engine` to configure isolated network. To communicate `docker network` uses drivers, which are :
- bridge
- host
- none

When working with `docker-compose`, it creates default network for itself. Although not required, yet it is good idea configure it.
```yaml
version: "2"
services:
  ninja:
    image: alpine
    command: sleep 10000
  pirate:
    image: alpine
    command: sleep 10000
networks: # optional if using only default network
  default:
    driver: bridge
```

---

# Default network (cont.)
Docker-compose inherits our current folder names when creating containers, thus it is good idea to create dedicate folder for it.

```sh
docker-compose up -d
# or
docker-compose --project-name default up -d
# docker-compose imports directory name
# we use --project-name to override the inherited name
```

---

# Practice

- Create a project folder named `stack` and in it add docker-compose.yml file.
- In docker-compose.yml add `web` and  `db` services with required ports
- Add default network configuration with bridge driver
- Run docker-compose command to test connection
- List docker networks

```yaml
version: "2"
services:
  web:
    image: nginx
    ports:
      - 8000:80
  db:
    image: redis
    ports:
      - 6379:6379
networks: # optional if using only default network
  default:
    driver: bridge
```

```sh
docker-compose up -d
docker network ls
```
---

# Isolating networks

It is also possible to choose to which networks out containers are connected and from which they are isolated from
here is an example of isolation

```yaml
version: "2"
services:
  ninja:
    image: alpine
    command: sleep 10000
    networks:
      - road
  pirate:
    image: alpine
    command: sleep 10000
    networks:
      - ocean
      - road
  noob:
    image: alpine
    command: sleep 10000
    networks:
      - ocean
networks: # The presence of these objects is sufficient to define them
  ocean: {} # placeholder for key value pairs
  road: {}
```
---
# Isolating networks (cont.)

Now, although previous example was not isolation per-se, it did separate the networks. Other way to isolate the network, would be to create docker network with `--internal` flag on docker network and connect with `external` tag in `docker-compose.yml`

```sh
docker network create  --internal --subnet 10.100.0.0/24 my-net
# network needs to be pre existing
```

```yaml
version: "2"
services:
  ninja:
    image: alpine
    command: sleep 10000
    networks:
      - road
  pirate:
    image: alpine
    command: sleep 10000
    networks:
      - ocean
      - road
networks: # The presence of these objects is sufficient to define them
  ocean: 
   external:
     name: my-net # remember that network needs to be existing
  road: {}
```

---

# Practice

- Create new project named isolated amd in it add docker-compose.yml
- Setup docker network named `isolated`
- In docker-compose:
    - Run web service based on nginx image opened on port 8000 on host
    - Run api service based on nginx image opened on por 8080 on host
    - Run db service based on redis image opened on port 6379 on host
    - Create 2 networks: front-tier and back-tier
    - Connect back-tier network to `isolated` network
    - Connect web and api services to all networks
    - Connect db service to back-tier network only
  - Test connection with ping
  - Install redis-cli on api service and connect to db service


```sh
docker network create  --internal --subnet 192.168.0.0/24 isolated
```
---

# Practice (cont.)

```yaml
version: "2"
services:
  web:
    image: nginx
    ports:
      - 8000:80
    networks:
      - back-tier
      - front-tier
   api:
    image: nginx
    ports:
      - 8080:80
    networks:
      - back-tier
      - front-tier 
  db:
    image: redis
    ports:
      - 6379:6379
    networks:
      - back-tier
networks: # optional if using only default network
  back-tier:
    external: 
      name: isolated
  front-tier: {}

```
---
# Practice (cont.)
```sh
docker-compose exec  api /bin/sh
apk update 
apk update redis-cli
ping -C 1 web
ping -C 1 api
pint -C 1 db
redis-cli -h db # if not connected means network is not working
```

---

# Using Volumes Docker Compose

## Overview

Volumes are persistent data stores implemented by the docker platform. The docker-compose specification offers a neutral abstraction for services to mount volumes, and configuration parameters to allocate them on infrastructure.

---

#  Simple volumes


Host path can be defined as an absolute or as a relative path.

Example:
```yaml
version '3'
 
services:
  app:
    image: nginx:alpine
    ports:
      - 80:80
    volumes:
      - /home/ubuntu/html:/usr/share/nginx/html:ro

```
---

# Practice

- Create new project named __simple-storage__ and add  `docker-compose.yml` and `default.conf` files in it.
- In `default.conf` append code below
    - It's nginx configuration file that servers on 80 and returns http code 200 with message
- In docker-compose create service named nginx
    - Use image named `nginx`
    - Setup volume file volume to link `default.conf` to `/etc/nginx/conf.d/default.conf`
    - Add ports to open port 80 to random port
    - Start the container with `docker-compose up -d`
    - Create new folder named `conf.d` and move `default.conf` there
    - Change file volume to folder volume in `docker-compose.yml`
    - Validate on which random port the container is running with `docker compose port nginx 80`
    - Test that container is accessible with `curl`
    - Inspect the container for existing volumes

---

# Practice (cont.)

```json
#default.conf to use
server {
   listen 80;
   server_name localhost;

   location / {
    return 200 "Hello World!";
   }
}
```

```yml
version '3'
 
services:
  app:
    image: nginx
    ports:
      - 80
    volumes:
      - ./default.conf:/etc/nginx/conf.d/default.conf
      #- ./conf.d:/etc/nginx/conf.d/ # this will be used after change
```

```sh
docker compose up -d
docker compose up -d # to restart after change
docker compose port nginx 80
docker localhost:32731
docker container ps
docker container inspect nginx # the container name might be different 
```
---

# Named volumes

Named volumes can be defined as internal (default) or external.

Docker internal named volumes

Docker compose internal named volumes have the scope of a single Docker-compose file and Docker creates them if they don’t exist.

Docker Compose file example with a named volume web_data:
```yaml
version '3'
 
volumes:
  web_data:
 
services:
  app:
    image: nginx:alpine
    ports:
      - 80:80
    volumes:
      - web_data:/usr/share/nginx/html:ro
volumes:
  web_data:
```
> `[!]` Notes:
> - From Docker Compose version 3.4 the name of the volume can be dynamically generated from environment variables placed in a .env file (this file has to be in the same folder as docker-compose.yml is).
> - To increase the security of our system we can mount the volume as read-only if the container only needs to read the mounted files. This will prevent an attacker/user to modify or create new files in the host of the server.

---
# Named volumes (cont.)


Example of .env file:
```sh
VOLUME_ID=my_volume_001
```
Example of a Docker Compose file with an internal docker named volume based on an environment variable:
```yaml
version '3.4'
 
volumes:
  web_data:
    name: ${VOLUME_ID}
 
services:
  app:
    image: nginx:alpine
    ports:
      - 80:80
    volumes:
      - web_data:/usr/share/nginx/html:ro
```
`docker-compose up -d` will generate a volume called my_volume_001.

---

# Practice
- Create new project named __named-storage__ and add  `docker-compose.yml` under it
- In `docker-compose.yml` add:
    - Use image `tutum/hello-world`
    - Create two services name `worker` and `reporter`
    - Add named volume `results`
    - Connect both of services to the same volume under `/results` mount point
    - Execute `touch` command to add test file with `worker` service
    - Validate with `ls` from reporter that you can access the same file
    - Check what named volumes exist
    - Export volumes from `worker` to `reporter` and make them read only

---

# Practice (cont.)

```yml
version: 2
services:
  worker:
    image: tutum/hello-world
    volumes:
      - results:/results
  reporter:
    image: tutum/hello-world
    volumes:
      - results:/results
volumes:
  results:

```

```sh
docker compose up -d
docker exec named-storage-worker-1  ls -la  /results
docker exec named-storage-reporter-1  ls -la  /results
docker exec named-storage-reporter-1  touch  /results/test
docker exec named-storage-worker-1   ls -la  /results
docker volume ls
```

---
# Practice (cont.)

```yml
version: 2
services:
  worker:
    image: tutum/hello-world
    volumes:
      - results:/results
  reporter:
    image: tutum/hello-world
    volumes_from:
      - worker:ro
volumes:
  results:
```
```sh
docker compose up -d
docker exec named-storage-reporter-1  touch  /results/test2 # should be error
```

---

# Docker external named volumes

Docker compose external named volumes can be used across the Docker installation and they need to be created by the user (otherwise fails) using the docker volume create command.

For example defines web_data volume:
```sh
docker volume create --driver local \
    --opt type=none \
    --opt device=/var/opt/my_website/dist \
    --opt o=bind web_data
```
`docker-compose.yml` file with a named volume web_data defined externally:
```yaml
version '3'
 
volumes:
  web_data:
    external: true
 
services:
  app:
    image: nginx:alpine
    ports:
      - 80:80
    volumes:
      - web_data:/usr/share/nginx/html:ro
```
There are different volume types like nfs, btrfs, ext3, ext4, and also 3rd party plugins to create volumes.

External named volumes can be defined dynamically from environment variables using a name section as we did in the previous example.

---

#  Practice

<!-- TODO Used course example from stoneriverlearning -->

---

# Docker compose builds

## Overview


---

# Docker-compose and dockerfile

docker-compose build
docker-compose up --build

```yaml
version: '3.9'
services:
  web:
    build:
      context: ./web
      dockerfile: ./web/build/Dockerfile-alpine
      args:
        environment: dev
        status: stable
    ports:
        - 8080:80
```
---

# Context

When the value supplied is a relative path, it is interpreted as relative to the location of the Compose file. This directory is also the build context that is sent to the Docker daemon.

Compose builds and tags it with a generated name, and uses that image thereafter.
```yaml
build:
  context: ./dir
```
That mean, its a path to the directory that contains Dockerfile file. It purposes to build a new image for the service.

---

# Practice

<!-- TODO Add from official documentation -->
---

# Summary Practice

- Fork and clone project from [From gitlab with name of Jenkins-Elementary](https://gitlab.com/vaiolabs-io/jenkins-examples.git)
- Run the application to verify that it is working
- Create the docker image that will include all dependencies from `requirements.txt` file
- Build that image save it docker hub
- create docker compose file that will have to running containers:
    - container app: application container that will receive connection on port 32223 on closed network
    - container web: nginx container that will send configuration to container `app`  but will receive communication from all networks

---

# Fin

As most of Italian comedies used to end with laughter while crying, I hope this course ends in the same manner. 
Hope you have Enjoyed and Learned a lot.
Wish you All the best and Hope to see you soon at my other courses.

What ever you do, do try to have some fun

Alex M. Schapelle

Vaiolabs.IO

Otomato.IO
