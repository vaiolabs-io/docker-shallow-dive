
---

# 03 Docker Management

---
# Docker Management

In this chapter we'll focus on dockers management commands. mainly on initials of docker.

The latest iteration of docker, have had some changes and as such, some of docker
commands have had changed their command line options. as such we'll start with management commands and their options.

---

# Docker Management Commands

The list of docker management commands is quite long and each command has its own sub-set of additional commands that can be passed. eventually every command and sub-command have additional options which we will discover  during our course. here is list of initial options.

- Builder: manage build
- Config: manage docker configs
- Container:  manage containers
- Engine: manage docker engine
- Image: manage docker images
- Network: manage docker/container networks
<!-- - Node: manage swarm nodes -->

---

# Docker Management Commands (cont.)

- Plugin: manage plugins
- Secret: manage docker secrets
- Service: manage services 
- System: manage docker
- Volume: manage volumes

Each and every one of those above have merged into sub commands and have additional
options to run with.

```sh
 docker image ls -h 
```

The command above will provide help on `ls` command under docker image

---

# Docker Management Commands (cont.)

Here is short list for several of the commands:

## Docker Image

- `ls`: list images
- `pull`: an image or a repository to a registry
- `push`: an image or a repository to a registry
- `inspect`: return low-level information on docker objects
- `import`: the contents from tarball to create filesystem image

---

# Docker Management Commands (cont.)

### Docker image list

We continue with list docker images on our host

```sh
docker image ls
```
In case the list is empty, don't worry, that it due to lack of images that will be fixed later

---

# Docker Management Commands (cont.)

### Docker image details

As mentioned before image is essentially stack of read-only file-systems, yet it also has additional data saved in image file and it can be described as follows:

```sh
docker image inspect IMAGE-ID
```

---

# Practice

- List  all images

```sh
docker image ls
```

---

# Practice (cont.)

- Delete all images if they exist
```sh
docker image ls 
docker image ls -q
docker image rm $(docker image ls -q)
```
---

# Practice (cont.)

- Pull next list of containers:
    - Fedora
    - Alpine
    - Nginx

```sh
docker image pull fedora
docker image pull alpine
docker image pull nginx
```

---

# Docker Management Commands (cont.)

## Docker Container

- `ls` : list container
- `run` : run command in a new container
- `inspect` : display detailed information on one or more containers
- `top` : display detailed information on one or more containers
- `restart` : one or more container
- `attach`: attach local stdin, stdout and stderr streams to a running container
- `stop` : one or more container
- `start`: one or more container

---

# Docker Management Commands (cont.)

## Docker Container

  - `log` : fetch the logs of a container
  - `stats` : display a live stream of container resource usage
  - `exec` : run a command in a running container
  - `pause` : pauses all processes with one or more containers
  - `unpause` : all processes with one or more containers
  - `rm` : remove one or more containers
  - `export` : export a containers filesystem as a tar archive
  - `prune` : remove all stopped containers

---

# Docker Management Commands (cont.)

## Docker Container

To run container from image, local or remote, we would usually for with __docker container run__. When creating containers it is worth to remember that when ever the container starts it executes its embedded command. once that command finishes.

Here are some examples simple `ubuntu/apache2` container:

```sh
 docker run ubuntu/apache2
```
It seems to be working, yet if we'll focus, we'll see that we do not have input and to container TTY


---

# Docker Containers

The options that can be added to __docker container run__ when we run it. By default each container can run as `--detach` or `-d`  for short.

```sh
 docker container run -d ubuntu
```

---

# Docker Containers

The other most useful container option would be combination of `--interactive` or `-i` with `--tty` or `-t`, which enables us to run container and directly log into the container itself:

```sh
docker container run -it ubuntu # this should log you into container and
# change your hostname to CONTAINER-ID
apt update && apt install -y vim # run
```
If you wish to exit from the container session with keeping container's process, push `[Ctrl+p]` and `[Ctrl+q]` keys.
> `[!]` Note: Might `NOT` work in web terminal

---

# Docker Container

## Executing Commands in Docker

Essentially every time we run containers, we run commands in them

```sh
docker container run -it  ubuntu
```

In cases where we would like to add additional commands or by pass internal CMD commands
we could use `exec` command for it.

```sh
 docker container exec -it  Container-Name /bin/bash
```
---

# Docker Container

## Executing Commands in Docker (cont.)

We can also pass several command via shell. although it needs the verification that correct shell is used.

```sh
 docker container exec -it  Container-Name  /bin/bash -c 'apt update ; apt install vim-y'
```

---

# Practice

- Run fedora based container in detach mode
```sh
docker container run -d fedora
```
---

# Practice (cont.)

- Run fedora based container in interactive mode and install vim
```sh
docker container run -it fedora
dnf install -y vim
```

---

# Practice (cont.)

- Exit the container and keep it running

```sh
[Ctrl+p] [Ctrl+q] # run while still in container >> might not work in web terminal
docker ps 
docker ps -a
```
---

# Practice (cont.)

- Execute command `cat /etc/*-release` within fedora container without connecting to interactive session

```sh
 docker container exec -it  Container-Name /bin/bash -c 'cat /etc/*-release'
```


--- 

# Practice (cont.)

- Run nginx container in detach 
```sh
docker container run -d nginx
```
--- 

# Practice (cont.)

- Validate that container is running
```sh
docker container ps # `container` keyword is optional because of it being default but useful
```
--- 

# Practice (cont.)

- Exec ubuntu container in interactive mode while running the `apt-get update && apt-get upgrade -y` command

```sh
# container must be running, else it won't work
docker container exec -it Container-Name /bin/bash -c 'apt-get update && apt-get upgrade -y '
```

---

# Docker container logging

Once we run container, it will continue to run, until embedded command or application in that container stops running for some kind of the reason. The `reason` can be check with `logs` sub command.

```sh
 docker container logs ubuntu
```

The docker logs command shows information logged by a running container. The docker service logs command shows information logged by all containers participating in a service. The information that is logged and the format of the log depends almost entirely on the container’s endpoint command.


---

# Docker Logging

There are also lots of cases when monitoring container to identify the errors in implanted application, and one must follow in live session, yet with out being connected directly to container, thus `--follow` or `-f` enables to follow log output

```sh
 docker container logs -f nginx
```
If we'll run `curl` command to that nginx container the logs with pop out with access

---

# Practice

- Run one liner script that counts till 100 and sleeps between iteration on ubuntu container
```sh
docker run -d  ubuntu /bin/bash -c 'i=0;while [[ i -lt 100 ]];do echo $i;((i++));sleep 1;done'
```

--- 

# Practice (cont.)

- Check output of the script with `logs` subcommand

```sh
docker container logs  CONTAINER-ID
```
--- 

# Practice (cont.)

- Follow script output as the script is running

```sh
docker container logs -f CONTAINER-ID
```

---

# Docker container management

As mentioned before, containers die, and when that happens we need to learn why it happened and fix them. Some time we need to rerun that same container. As discussed at the beginning, container is an instance of image and it consists of read only filesystem __with running process of application__

That process can be restarted, paused and even brought back to run status.

```sh
docker container ps
docker container pause CONTAINER-ID
docker container unpause CONTAINER-ID
```
---
# Docker container management (cont.)

The other cases of container management can be described as follows:

```sh
docker ps # container word is optional as mentioned before
docker stop CONTAINER-ID # stops  the running gracefully with SIGTERM
docker kill CONTAINER-ID # stops  the running gracefully with SIGKILL
docker rm CONTAINER-ID # removes container 
docker start CONTAINER-ID # starts the running
```


---

# Practice

- Validate that nginx container is still running
```sh
docker ps
```
--- 

# Practice (cont.)

- If it doesn't start it.
```sh
docker ps -a 
docker start CONTAINER-ID # this will start previous container 
```
--- 

# Practice (cont.)

- Check logs of running container continuously.
```sh
docker container logs CONTAINER-ID
```
--- 

# Practice (cont.)

- Observe the logs while accessing the container.
```sh
docker container logs -f CONTAINER-ID
```
- Try accessing the container via `curl`, `wget` or browser.
```sh
curl CONTAINER-ID  # the port depends on the exported port
```
---

# Summary Practice

- With one line commands, stop all containers.
- Delete all images on your host.
- Pull next list of containers:
    - Debian
    - Rocky
    - Nginx

---

# Summary Practice (cont.)

- Run nginx container 
- Validate that container is running.
- Check for container logs.
- Search for python3 container while presenting format of name and limiting search to 100.
- Run container of Rocky to print out /etc/*release in detached and interactive mode.
- __BONUS__ : Please create script that will run 3 containers with different predefined
               names under specific network that was created with your shell script
