
---

# 02 Docker Architecture

<img src="../99_misc/.img/container_arch.jpeg" alt="cgroups/namespaces  " style="float:left;width:400px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/container_arch.jpeg) -->


---
# Initial Practice

- Run your first docker container command to kick-off this course with container name hello-world

```sh
docker run hello-world
```
let's describe what happened her first

---
# Docker Architecture

<img src="../99_misc/.img/cgroup_namespace.png" alt="cgroups/namespaces  " style="border-radius:25px;float:right;width:400px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/cgroup_namespace.png) -->

Docker utilizes some of Linux kernel features, in order to function:

 - CGroups (what you can use)
 - Namespaces (what you have)
 - Union filesystem
 - Kernel capabilities (what you are allowed to do by kernel )

---

# Docker Arch: CGroups (cont.) 

CGroups is Linux kernel feature that limits, accounts for, and isolates resources of collection processes
this allow management of resource per container:

- Memory
- Cpu
- Network
- Disk bandwidth

---

# Docker Arch: Namespaces (cont.) 

Namespaces are layer of isolation: each aspect of container runs in its own namespace and does not have access outside it

- The __`pid`__ namespace: used for process isolation
- The __`net`__ namespace: used for managing network interfaces
- The __`ipc`__ namespace: used for managing inter-process communication
- The __`mnt`__ namespace: used for managing mount points
- The __`uts`__ namespace: used for managing kernel identifiers (unix timesharing system)
- The __`user`__ namespace: used for managing containers from user space.

---

# Docker Arch: Union File Systems (cont.) 


- Unionfs, or Union filesystem are the are filesystems that operate by creating layers
- Docker uses union filesystems to provide the building blocks for containers
- Docker can make use of several union file system variants include: aufs, btrfs, vfs, devicemapper and many more.
- The most popular is aufs
- Since ubuntu 16.04 the default filesystem on ubuntu is overlayfs

What is it used for, though ? It is the glue that connects container internals in addition of namespaces and cgroups

---

# Docker Arch (cont.)

- The docker client(docker):
  - Is how users interact with docker
  - the client sends these commands to dockerd
- Docker registry:
  - stores docker images
  - public registry such as docker-hub
  - run own private registry


---

# The Docker Engine

## Modular In Design

  <img src="../99_misc/.img/docker-eng0.png" alt="drawing" style="border-radius:25px;float:right;width:500px;">
<!-- ![bg right:40% width:500px](../99_misc/.img/docker-eng0.png) -->

- Batteries included but replaceable
- Based on open-standards outline by the open container initiative
- The major components:
    - docker client
    - docker daemon

---

# The Docker Engine (cont.)


-  The components work together to create and run containers
-  A brief illustration:
  
  <img src="../99_misc/.img/docker-arch0.png" alt="arch" style="border-radius:25px;float:right;width:760px;">
<!-- ![bg right:55% contain](../99_misc/.img/docker-arch0.png) -->

---

# The Docker Engine (cont.)

- Initially based on lxc project to use Linux specific tools
 - Namespaces
 - Control groups (cgroups)

Dockers first release was huge monolithic mess, all in one binary that included:

 - docker client
 - docker daemon
 - docker api
 - container runtime
 - image builds

---

# The Docker Engine (cont.)

- Lib-lxc was later replaced with `libcontainer`
- Docker 0.9 included new version of `libcontainer`
- It also was platform agnostic(working all os types)
- But it was still monolithic:
    - Slow
    - Harder to innovate
    - Not what ecosystem wanted/needed ...
- This opened the door out of box thinking:
    - Smaller and specialized tools
    - Plug-able architecture

---

# The Docker Engine (cont.)

## Open Container Initiative (OCI)

A group that standardized container requirement need to for ecosystem and development.
the idea suggested were:

  - Image spec
  - Container runtime spec
  - Version of standard 1.0 released in 2017
  - Docker inc., heavily contributed to all those specs
  - By the version of 1.11 of docker(2016) most of these specification were mostly Implement in docker container environment.

---

# The Docker Engine (cont.)

## Running Containers

```sh
docker container run -it --name <NAME> <IMG>:<TAG>
```


Creating a container:

- Using cli to execute command
- Docker client uses the appropriate api payload
- POSTs to the correct api endpoint
- The docker daemon receives instructions
- The docker daemon calls containerd to start new container
- Docker daemon uses __`grpc`__ (CRUD like api)

---

# Docker Images and Containers

## What is docker image?

- Essentially, it is a template of union filesystem layers, stacked together, for creating an image.
    - If you are familiar with OOP, one can treat them as classes.
- Containers are instances of __`image`__

---

# Docker Images and Containers

## What is docker image? (cont.)

- In some sense, images are stopped containers
- Images are comprised of multiple layers
- Images are considered as build time constructs, while containers are considered as run time constructs
> `[!]` NOTE: It worth mentioning that __`images`__ and __`containers`__ are dependent on each other, meaning that if you wish to remove docker image, you'll have delete all the containers before deleting that image.

---

# Docker Images and Containers (cont.)


## Images are build from __`dockerfiles`__
  - This file includes instructions on how that container should be set up.
  - The same instructions holds the list of binaries needed to be installed for the application to run, for example for python based web service written with flask, you'll probably use to install python and flask binaries only.
  <!-- - Images are made of layers
  - All the layers are readonly, besides the last one, which is only layer that can be changes/edited.
    - If you login to container and install __vim__ the binary of it will only be install on the last layer that is editable. it will __NOT__ be added to the image or any layer of image.
  - Here is an example of image layers --> 
  <img src="../99_misc/.img/docker-layers.png" alt="drawing" style="border-radius:25px;float:right;width:400px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/docker-layers.png) -->


---

# Docker Images and Containers (cont.)


- It is also worth mentioning that different containers can use same image at the same time.
- Just look on the illustration:
  
<img src="../99_misc/.img/docker-multi-container-layers.png" alt="drawing" style="border-radius:25px;float:right;width:400px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/docker-multi-container-layers.png) -->


---

# Docker Hub

## What Is Docker Hub?
<img src="../99_misc/.img/docker_hub.png" alt="drawing" style="border-radius:25px;float:right;width:180px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/docker_hub.png) -->

- It is public registry: think of it as git but for containers
- Service provided by docker
- Features:
    - Repositories
        - Place to keep your builds and share them with users
    - Teams and organizations
        - Level of managements that you can achieve.

---

# Docker Hub (cont.)

- Official images
    - Base images created by docker to have base images to start from.
- Publisher images
    - List of high quality images provided by 3rd party publishers, for example python container provided by python software foundation.
- Docker hub can build images and push it to your repository
- Web-hooks: to trigger custom changes per your build image.
- We'll be using docker hub, so sign up: `https://hub.docker.com/signup`


---
# Summary Practice

- Run similar command to initial practice yet use nginx container image

```sh
docker run nginx
```
>  Please do describe what is happening

>  Please do ask questions

