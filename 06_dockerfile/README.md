
---
# 06 Dockerfile
<!-- ![bg right:40% fit](../99_misc/.img/dfile.png) -->

---

# Intro To The Dockerfile
<img src="../99_misc/.img/dfile.png" alt="dockerfile" style="border-radius:25px;float:right;width:400px;">


### What is dockerfile?

Docker files are just simple text files with set of instructions on how to build or append an docker image.

The file contains all the knowledge we have discussed up until know, and it also uses commands used to initialize a container:

- Read-only layers of file-system for immutabillity.
- Each layer representing a dockerfile instruction.

---

# Intro To The Dockerfile (cont.)

### What is dockerfile?  (cont.)

- Layers are stacked upon each other.
- Each layer is delta of change from the previous layer
- Images are build with `docker image build` command

---

# Intro To The Dockerfile (cont.)

General guidelines for dockerfile creation:

- Keep containers as ephemeral as possible, only include required binaries.
- Avoid including unnecessary files.
- Use __.dockerignore__ to dismiss parts of application, you do not wish to be included in your image.
- Decouple application, no monoliths inside containers.

---

# Intro To The Dockerfile (cont.)

- Use multi-stage container builds.
- Minimize number of layers
- Sort multi-line arguments
- Leverage the builds

---

# Working With Instructions

Here is a mostly complete list of docker file commands.

- __FROM__: Chooses the base image for subsequent instructions
- __RUN__: Executes any commands while docker image will be created.
- __CMD__: Sets default parameters that can be overridden from the Docker Command Line Interface (CLI) when a container is running.

---

# Working With Instructions (cont.)

- __ENTRYPOINT__: Default parameters that cannot be overridden when Docker Containers run with CLI parameters.
- __LABEL__: Adds metadata to an image.
- __EXPOSE__: Informs docker that the container will listen on the specified network ports at runtime.


---

# Working With Instructions (cont.)

- __ADD__:  Copies new files, directories or remote file urls.
- __COPY__: It copies new files or directories from host to image file-systems. 
- __WORKDIR__: Sets the working directory
- __ENV__:  Provides the environment variable


---

# Working With Instructions (cont.)

- __ARG__: Enables to pass arguments to be used as value in dockerfile
- __USER__: The USER instruction is used to define the default user or gid when running the image. 
    - The __RUN__, __CMD__, and __ENTRYPOINT__ follow the USER instruction in the Dockerfile
- __VOLUME__: The VOLUME instruction ad used to enable access/linked directory between the container and the host machine. 

---

# Working With Instructions (cont.)

Lets start with something simple:
 
Simple dockerfile that copies __FROM__ ubuntu image, uses __RUN__ to get updated and install apache2 web-server. by the, it runs __CMD__ apache web server with `apachectl` with parameters. Let's see the example and explain little bits and bytes :


```docker
FROM ubuntu:20.04
RUN apt-get update && apt-get -y install apache2 
CMD apachectl -D FOREGROUND
```

```sh
 docker build .
```

---

# Working With Instructions (cont.)

We can build it with command below:

- __-t__ is used to create tags on images

```sh
 docker build -t apache2:v1 .
```

Congratulations: you are now a proud owner of a simple image named `apache2:v1`.

> `[!]` Note: Do NOT forget `dot` at the end of the command line 

---

# Practice

- Create folder named __alpine-env__ and in it add dockerfile
- In dockerfile add instruction to use alpine as basis of your image
- Require to update alpine image repositories
- Request to install vim, git, curl
- Build image with tag `my-first-alpine:v1`
- Run container from `my-first-alpine:v1` image

---

# Practice (cont.)

```sh
mkdir alpine-env; touch alpine-env/dockerfile
```

```dockerfile
FROM alpine
RUN apk update && apk add vim \
    git \
    curl # because we want to have as less layers as possible
CMD echo "The container has finished and can go home"
```

```sh
docker build -t my-first-alpine:v1 .
docker run -d my-first-alpine:v1
```

---

# Working With Instructions (cont.)

Now lets do expand little bit. Let's add environment variables with __ENV__ keyword and arguments with __ARG__

```docker
FROM ubuntu 
ARG TZ_DATA
ENV DEBIAN_FRONTEND="noninteractive" 
ENV TZ=${TZ_DATA}
RUN apt-get update && apt-get -y install apache2
CMD apachectl -D FOREGROUND 
```
---

# Working With Instructions (cont.)

And build it with different syntax of `build` command

```sh
 docker build -t apache2:v2 --build-arg USER=ninja .
```
> `[!]` DO NOT PASS PASSWORDS/SECRETS/SSH_KEYS - all the passed data is visible in `docker history` command

---

# Practice

- Create alpine image that runs nginx service 
- Set `USER` argument to pass to build
- Set TZ environment variable to America/Chicago
- Set USER environment variable to ninja
- Build the docker image with tag `my-alpine-nginx:0.0.1` and pass argument ninja to `USER`
- Run the container from created image
---

# Practice (cont.)

```dockerfile
FROM alpine
ARG USER
ENV TZ=America/Chicago
ENV USER=${USER}
RUN apk update && apk add nginx
CMD nginx -g daemon off;
```

```sh
docker build -t my-alpine-nginx:0.0.1 --build-arg USER=ninja . # do not forget the DOT
docker run -d my-alpine-nginx:0.0.1 # if version is not provided on the latest will be used
```
---

# Working With Instructions (cont.)


Here is a small example of single html file application that only shows time and with it we have simple nginx based container that we implement in order create container for our use.

```docker

FROM nginx 
LABEL maintainer="io.vaiolabs.botex"
LABEL version="v0.1.0"
COPY index.html /var/www/html/ 
EXPOSE 8080 
```

---

# Practice

- Create dockerfile with import from ubuntu:18.04
- The image should have Labels of maintainer, version and description
- Environment variable named `DEBIAN_FRONTEND` should be set to `noninteractive`
- The image should install nginx, php-fpm and supervisor
- Clean up all the packages from image
- Expose port 443 and 80 
- Create and Copy *index.html* to `/usr/share/nginx/html/` in the image
- Run nginx command
- Build the image named `custom-lamp`

---

# Practice (cont.)

```dockerfile
# Download base image ubuntu 18.04
FROM ubuntu:18.04

# LABEL about the custom image
LABEL maintainer="Alexm@Otomato.io"
LABEL version="0.1.0"
LABEL description="This is custom Docker Image for \
the PHP-FPM and Nginx Services."

# Setting up ENV variable so that apt-get won't be interactive
ENV DEBIAN_FRONTEND=noninteractive
# Update Ubuntu Software repository
# Install nginx, php-fpm and supervisord from ubuntu repository
RUN apt-get update && apt-get install -y nginx php-fpm supervisor && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean
# Expose Port for the Application 
EXPOSE 80 443
COPY index.html  /var/www/html/
CMD ["nginx", "-g", "daemon off;"]
```
---

# Practice (cont.)

```sh
touch index.html
```

```html
<h1> Hello World </h1>
```

---

# Working With Non-Privileged User

When working with containers security is something we all wish to comply with. as such, some of the security measures that are taken in order to assure that __root__ user inside the container will not be implemented. as such we have __USER__ commands that we can use to assure it

---

# Working With Non-Privileged User (cont.)

For example:

```docker
FROM ubuntu:20.04 
RUN useradd -ms /bin/bash app_user
USER app_user 
```

> `[!]` Note: once the docker image with __non-root__ user is created, everything in that container will run, under that specific user. if __su__ is implemented, you won't have password, thus won't be able to login. if __sudo__ is implemented, it will be denied by the container itself.

> `[!]` Note: it will be possible to access the container with __root__ from *docker* command with use of __exec -u 0__ while creating the container.

---

# Order Of Executions

Obviously we have gone through some commands already one thing still remains a question: is there any type of hexarchy between commands ?
To be precise, there is none. any command be executed at any time. the __problem__ that can occur is problem bad planning. for example:

```sh
i=100
while [[ $i -gt 0 ]]
  do
    echo "Shell application still Running... $i"
    sleep 10
    ((i--))
  done
```
---

# Order Of Executions (cont.)

```dockerfile
FROM rockylinux:8.6
RUN useradd -m -d /home/app_user -s /bin/bash app_user
USER app_user
WORKDIR /home/app_user
COPY app.sh /home/app_user/app.sh
CMD ["bash", "/home/app_user/app.sh" ]
```

In example above we are creating local user name *app_user*. after the user is created, all the commands are executed with that user. but because of that user being regular user, he does not have any permissions to work on anything. thus the build of that image will fail.


---

# Practice

- Lets create py-webserver project:
    - Create py-web-server.py file and paste into it next code:
- Create dockerfile that will containerize the python web server 
  -  Import from python3 container
  -  Create Labels with maintainer, version and description
  -  Create local user at the image name *snek*
  -  Create workdir for the application to run:wq
  -  Expose port 8000 for communication

---

# Practice (cont.)

  -  Copy the py-web-server to workdir folder
  -  Run the application with python3 
  -  Build the image  and tag it as `py-web-server:0.0.1`
  -  Run the image and open port to host 8000
  -  Try to access via localhost:8000 with `curl`
  -  Check the logs output

---

# Practice (cont.)

```py
from http.server import HTTPServer, SimpleHTTPRequestHandler

def run(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler):
    """Entrypoint for python server"""
    server_address = ("0.0.0.0", 8000)
    httpd = server_class(server_address, handler_class)
    print("Staring web-server...")
    httpd.serve_forever()

if __name__ == "__main__":
    run()
```
---

# Practice (cont.)

```dockerfile
FROM python:3.9
RUN useradd -m -d /home/app_user -s /bin/bash app_user
USER app_user
WORKDIR /home/app_user
EXPOSE 8000
COPY py-web-server.py /home/app_user/app.py
CMD ["python3", "/home/app_user/app.py" ]
```
---

# Practice (cont.)

```sh
docker build -t py-web-server:0.0.1 .
docker run -d -p 8000:8000 py-web-server:0.0.1
docker inspect CONTAINER-ID # for ip address
curl localhost:8000
docker logs CONTAINER-ID
```
---

# Entrypoint VS. CMD

Let's address the ENTRYPOINT in the room ...

- `CMD` is defined as main command of running container
    - It's main use to be main process in the container
    - It is easily overridden
    - You may have multiple `CMD`s in your file, yet only the last one will be used

---

# Entrypoint VS. CMD (cont.)

- `ENTRYPOINT` defines a container with a specific executable
    - It can __NOT__ be overridden once the container starts
    - There is, however, a by pass flag called `--entrypoint` but it is considered as hack

The rule of thumb would be to use CMD for easily set env variables and ENTRYPOINT to run the executables

---

# Entrypoint VS. CMD (cont.)

## Shell and Exec
All Docker instructions types (commands) can be specified in either shell or exec forms

##### Shell form

```dockerfile
RUN         yum -y update
RUN         yum -y install httpd
COPY        ./index.html /var/www/index.html
CMD         echo “Hello World”
```
---

# Entrypoint VS. CMD (cont.)

##### Exec form
```dockerfile
RUN         yum -y update
RUN         yum -y install httpd
COPY        ./index.html /var/www/index.html
CMD         ["echo", “Hello World”]
```
---

# Entrypoint VS. CMD (cont.)

- So what's the difference ?
    - Shell form invokes shell, usually depending on main shell of container,
     mostly will be `bash`, but also `sh` in cases of slim containers
    - Exec form runs command directly in the specific environment, 
    which leaves shell variables, path and other configurations out of access,
    unless configured with __ENV__ 

---

# Practice

 - Go to previous practice and change CMD to ENTRYPOINT
 - Build the image named py-web-server:0.0.2
 - Run the container from that image
 - Try to Override the command with external command in `-it` mode --> it should fail

---

# Practice (cont.)

```dockerfile
FROM python:3.9
RUN useradd -m -d /home/app_user -s /bin/bash snek
USER snek
WORKDIR /home/snek
EXPOSE 8000
COPY py-web-server.py /home/snek/app.py
ENTRYPOINT ["python3", "/home/snek/app.py" ]
```
---

# Practice (cont.)

```sh
docker build -t py-web-server:0.0.2 .
docker run -it py-web-server:0.0.2 /bin/bash # this should start the web server with any shell
```

---

# Using `.dockerignore`

Just like with __git__, while working on __dockerfile__ wen encounter situations in which some of the files that  we work on, either are not needed  orw were created for testing purpose only, thus we need to discard them. in big projects, removing un-necessary files might become tedious, so __.dockerignore__ file was introduced. main point of it is to hold the list of things you do not wish to be included in you docker image.
it usually looks like this:

```sh
 vi .dockerignore
```
---

# Using `.dockerignore` (cont.)

The content inside the file can be written in several ways just like in __.gitignore__

```dockerignore
*.md
tests/*
```

---

# Practice

- Create Dockerfile and .dockerignore
- Setup dockerignore file to never include:
    - env-file
    - secrets-file
    - `.git` folder
    - `tests` folder
- Build dockerfile 
- Verify that none of the above are included

---

# Multi-stage builds
<!-- TODO provide example of multiple containers -->
### Before multi-stage builds

One of the most challenging things about building images is keeping the image size down. Each RUN, COPY, and ADD instruction in the Dockerfile adds a layer to the image, and you need to remember to clean up any artifacts you don’t need before moving on to the next layer. To write a really efficient Dockerfile, you have traditionally needed to employ shell tricks and other logic to keep the layers as small as possible

---

# Multi-stage builds
<!-- TODO provide example of multiple containers -->
### Before multi-stage builds (cont.)

It was actually very common to have one Dockerfile to use for development, and a slimmed-down one to use for production, which only contained your application and exactly what was needed to run it. This has been referred to as the "builder pattern".

---

# Multi-stage builds (cont.)

With multi-stage builds, you use multiple FROM statements in your Dockerfile. Each FROM instruction can use a different base, and each of them begins a new stage of the build. You can selectively copy artifacts from one stage to another, leaving behind everything you don’t want in the final image.

#### Lets use example app in C

```c
#include <stdio.h>


int main(){

        printf("\nHello World\n");
        //  very complex application
    
    return 0;
}
```

```docker
# syntax=docker/dockerfile:1

FROM alpine AS BUILDER
WORKDIR /home/project
RUN apk update --no-cache && apk add git automake autoconf alpine-sdk ncurses-dev ncurses-static
COPY app.c 
RUN gcc app.c -o app

FROM alpine:latest  
WORKDIR /home/app_home
CMD ["./app"]
```
---

# Multi-stage builds (cont.)


---

# Multi-stage builds (cont.)

How does it work? The second FROM instruction starts a new build stage with the alpine:latest image as its base. The COPY --from=0 line copies just the built artifact from the previous stage into this new stage. The Go SDK and any intermediate artifacts are left behind, and not saved in the final image.

---

# Multi-stage builds (cont.)

### Name your build stages

By default, the stages are not named, and you refer to them by their integer number, starting with 0 for the first `FROM` instruction. However, you can name your stages, by adding an `AS <NAME>` to the `FROM` instruction. This example improves the previous one by naming the stages and using the name in the `COPY` instruction. This means that even if the instructions in your Dockerfile are re-ordered later, the `COPY` doesn’t break

---

# Multi-stage builds (cont.)

### Name your build stages

```docker
# syntax=docker/dockerfile:1

FROM golang:1.16 AS builder
WORKDIR /go/src/github.com/alexellis/href-counter/
RUN go get -d -v golang.org/x/net/html  
COPY app.go ./
RUN CGO_ENABLED=0 go build -a -installsuffix cgo -o app .

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/github.com/alexellis/href-counter/app ./
CMD ["./app"]  
```
The end result is the same tiny production image as before, with a significant reduction in complexity. You don’t need to create any intermediate images, and you don’t need to extract any artifacts to your local system at all


---

# Practice

- Create Dockerfile 
    - In file, setup 2 step build based on alpine container: 
        - Use `cmatrix` project from github repository: https://github.com/abishekvashok/cmatrix
        - Run alpine container and in it create environment suitable for building project mentioned above
        - Use all the commands ran in container to create `Dockerfile`
        - Build the container with those commands and check its size
            - In case it is bigger then 15 MB:
              -  Use multi-container build with alpine container
              -  Build the `cmatrix` app on `builder` container
              -  Copy the app binary to new container
              -  Execute the binary of new app

---

# Practice (cont.)

```docker
FROM alpine as cmatrixbuilder
WORKDIR cmatrix
RUN apk update --no-cache && \
    apk add git autoconf automake alpine-sdk ncurses-dev ncurses-static && \
    git clone https://github.com/abishekvashok/cmatrix && \
    mkdir -p /usr/lib/kbd/consolefonts /usr/share/console/fonts && \
    ./configure LDFLAGS="-statuc" && \
    make

FROM alpine
RUN apk update --no-cache && apk add ncurses-terminfo-base
COPY --from=cmatrixbuilder /cmatrix/cmatrix /cmatrix
CMD ["./cmatrix"]

```
---


# Summary Practice

Describe what the next dockerfile does:

```dockerfile
# Download base image ubuntu 20.04
FROM ubuntu:20.04
LABEL maintainer="admin@sysadminjournal.com"
LABEL version="0.1"
LABEL description="This is custom Docker Image."
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update
RUN apt install -y nginx php-fpm supervisor && \
    rm -rf /var/lib/apt/lists/* && \
    apt clean
ENV nginx_vhost /etc/nginx/sites-available/default
ENV php_conf /etc/php/7.4/fpm/php.ini
ENV nginx_conf /etc/nginx/nginx.conf
ENV supervisor_conf /etc/supervisor/supervisord.conf
COPY default ${nginx_vhost}
RUN sed -i -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' ${php_conf} && \
    echo "\ndaemon off;" >> ${nginx_conf}
COPY supervisord.conf ${supervisor_conf}
RUN mkdir -p /run/php && \
    chown -R www-data:www-data /var/www/html && \
    chown -R www-data:www-data /run/php
VOLUME ["/etc/nginx/sites-enabled", "/etc/nginx/certs", "/etc/nginx/conf.d", "/var/log/nginx", "/var/www/html"]
COPY start.sh /start.sh
CMD ["./start.sh"]
EXPOSE 80 443
```
<!-- TODO  Needs something practical -->