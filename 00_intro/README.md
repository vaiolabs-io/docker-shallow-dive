
# Docker Shallow Dive

.footer: Created By Alex M. Schapelle VAioLabs.io


---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is docker ?
- Who needs docker ?
- How docker works ?
- How to manage docker in various scenarios ?


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of containers
- for junior/senior developers who are still developing on LAMP/LEMP/WAMP/MAMP stacks
- Experienced ops who need refresher


---

# Course Topics

- Intro
- Docker internals
- Docker basics:
    - Networking
    - Storage
    - Docker images
    - Dockerfile


---
# Course Topics (cont.)

- Docker basics:
    - Building and distributing images
    - Managing images
- Beyond basics
      - Docker Compose

---
# About Me
<img src="../99_misc/.img/me.jpg" alt="drawing" style="border-radius:25px;float:right;width:180px;">
<!-- ![bg right:20% contain](../99_misc/.img/me.jpg) -->

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
<img src="../99_misc/.img/me2.png" alt="drawing" style="border-radius:25px;float:right;width:180px;">

- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

---
# About Me (cont.)

<img src="../99_misc/.img/me3.jpg" alt="drawing" style="border-radius:25px;float:right;width:180px;">

You can find me on the internet in bunch of places:

- Linkedin: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)
- Gitlab: [Silent-Mobius](https://gitlab.com/silent-mobius)
- Github: [Zero-Pytagoras](https://github.com/zero-pytagoras)
- ASchapelle: [My Site](https://aschapelle.com)
- VaioLabs-IO: [My company site](https://vaiolabs.io)


---

# About You

Share some things about yourselves:

- Name and Lastname
- What do you do ? Not only job description
- What type of education do you poses ? formal/informal/self-taught/university/cert-course
- Do you know any of those technologies below ? What level ?
    - Containers: Docker / Docker-Compose / K8s
    - CI/CD: Jenkins / GitLab / Github / Gitea / Bitbucket
    - Scripting/Programming : Bash/ PowerShell / Python/ Javascript
- Do you have any hobbies ?
- Do you pledge your alliance to [Emperor of Man kind](https://warhammer40k.fandom.com/wiki/Emperor_of_Mankind) ?

---

# History 101

## Software Development Environment Dilemma: Where to develop ?

Three main options are:

- Local Development
- Development At Virtual Machines
- Containers: LXc, Docker, Podman and etc.


---

# Local Development
<!-- mention stackoverflow survey -->
<img src="../99_misc/.img/survey.png" alt="survey" style="border-radius:25px;float:right;width:400px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/survey.png) -->

Installing all in your working laptop/desktop to achieve __Dev-Prod Parity__



---

# Local Development (cont.)
Here is list of problems with that:

- UNIX/Linux and Windows differences
    - File encoding
    - Case sensitive
    - Permission
- In windows, things may not work, as example, windows 7 to 10 upgrade
- Once the project gets bigger, working on your direct box might become inconvenient

How to fix it all ? Well, next natural step is to move on to __Virtual Environments__

---

# Development At Virtual Machines

By running with virtualized environment with tools like __VirtualBox, VMWare, KVM or Parallel__. 
one can even argue, that it can be easily automated with tools such as Vagrant,
to which you can save code separately.

---

# Development At Virtual Machines (cont.)

All seems fine until others will join your work and then these start to happen:

- Configuration drift, meaning that current config will be different from what you started with.
- Mutable infrastructure, suggesting that your infrastructure might change while running in production and that might have dangerous effect applications running it them.
  
---

# Development At Virtual Machines (cont.)

- Maintains updates of VM is also an issue that requires attention of specialized team which in cases of start-ups is not an case.
- It all can be automated with software such as __vagrant__, adding another layer of complexity does not always solves your initial problem and also might become bigger issue in future.

Working on laptops, has additional effect on virtual environments. my laptop with 8 cores of cpu and 32 gb ram,
with suitable storage will run the virtual environment differently from some elses laptop who has only 2 cores of cpu 8gb ram.

---

# Development At Virtual Machines (cont.)

So there is no more use cases for VMs? well not entirely. VMs are still part of our life and they will continue to be so for long but use cases are shifting from what they used to be.

In order to involve from this place, solution was provided in shape of __container__, which is next natural step.

---

# Containers

<img src="../99_misc/.img/container_yard.jpg" alt="container" style="border-radius:25px;float:right;width:400px;">
<!-- ![bg right:40% width:400px height:400px](../99_misc/.img/container_yard.jpg) -->

__Definition__: a __container__ is an object for holding or transporting something.

This is also the use case with software development. 

---

# Containers (cont.)

Containers provide:

- __Immutability__, which was an issue with VM.
- They only contain libraries and binaries of required software.
- Code or external things that might need change, are inserted from external sources.

---

# Containers (cont.)

Why is it so important ?
By separating data, libraries, binaries and files, we are creating immutable services. if changes need to happen, we take down the container and bring up the new one, __configuration drift__ can never happen.

---

# Containers (cont.)

As established at `Twelve-Factor App methodology`, [Dev-Prod parity](https://en.wikipedia.org/wiki/Twelve-Factor_App_methodology), because containers can not change, what ever works in __Dev__, same will work in __Prod__. 

#### What about updates and patches ?
Well the thing that is updated is images, but from container point of view, they are read-only file-systems, but those do not have effect on main process of binary running inside of container.

#### Main binary in side of container ? 
Yes, container only run what is required and nothing else.

---

# Why Not Vm's Or Bare Metal?

<img src="../99_misc/.img/vm2.png" alt="drawing" style="border-radius:25px;float:right;width:400px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/vm2.png) -->

In traditional virtualization, a hypervisor runs virtual OS on physical hardware. The result is that each virtual machine contains a guest OS, a virtual copy of the hardware that the OS requires to run and an application and its associated libraries and dependencies. VMs with different operating systems can be run on the same physical server. For example, a VMware VM can run next to a Linux VM, which runs next to a Microsoft VM, and so on.

---

# Why Not Vm's Or Bare Metal? (cont.)

Instead of virtualizing the underlying hardware, containers virtualize the operating system (typically Linux or Windows) so each individual container contains only the application and its libraries and dependencies. Containers are small, fast, and portable because, unlike a virtual machine, containers do not need to include a guest OS in every instance and can, instead, simply leverage the features and resources of the host OS. 

Just like virtual machines, containers allow developers to improve CPU and memory utilization of physical machines. Containers go even further, however, because they also enable microservice architectures, where application components can be deployed and scaled more granularly. This is an attractive alternative to having to scale up an entire monolithic application because a single component is struggling with load


---

# Container VS. VirtualMachine


---

# What Is Docker ?
<!-- Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. Because all of the containers share the services of a single operating system kernel, they use fewer resources than virtual machines -->
1) Company:
<img src="../99_misc/.img/dotcloud.png" alt="dotcloud" style="border-radius:25px;float:right;width:300px;">
<!-- ![bg right:40% width:300px](../99_misc/.img/dotcloud.png) -->

- Docker inc.
    - Based in San Francisco
    - Founded by Solomon Hykes
    - Started as PaaS provider named dotCloud
    - Leveraged LXC
    - Their internal tool used to manage containers named docker
    - In 2013 they re-branded to Docker

---

# What Is Docker ? (cont.)

2) Container runtime and orchestration engine
<img src="../99_misc/.img/docker.png" alt="docker" style="border-radius:25px;float:right;width:300px;">
<!-- ![bg right:40% width:300px](../99_misc/.img/docker.png) -->

- Most people are referring to the docker engine:
    - 2 main editions:
         - Enterprise Edition (EE)
         - Community Edition (CE)
    - Both are released quarterly:
         - EE: supported for 12 month
         - CE: supported for 4 month

---

# What Is Docker ? (cont.)

3. Docker open-source project
<img src="../99_misc/.img/moby.png" alt="moby" style="border-radius:25px;float:right;width:300px;">
<!-- ![bg right:40% width:400px](../99_misc/.img/moby.png) -->

- Also called Moby
  - Upstream project of docker
  - Breaks docker down into more modular components



