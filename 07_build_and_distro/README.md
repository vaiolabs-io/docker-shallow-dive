
---

# 07 Building and Distribution Images

---

# Building Images 


There are some flags that can be very useful while building the images and those are:

- *-f* or *--file* : name of the Dockerfile
- *--force-rm*: always remove intermediate containers
- *--label*: set meta-data for an image
- *--rm* removes intermediate containers after a successful build
- *--ulimit*: user space limits for containers

An example:

```sh
docker image build -t vaiolabs/test:build-test --label io.vaiolabs.version=v0.9 -f Dockerfile.current .
```

*Dockerfile.current* is just a specific docker file.(in case you'd like to have several docker files on the project)
