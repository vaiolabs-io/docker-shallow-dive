
---

# 05 Docker Storage
<img src="../99_misc/.img/docker-storage.png" alt="drawing" style="border-radius:25px;float:right;width:300px;">
<!-- ![bg right:40% contain](../99_misc/.img/docker-storage.png) -->

---

# Intro To Docker Storage 

- Non-persistent :
    - Data that is ephemeral
    - Every container has it
    - Tied to the lifecycle of the container
- Persistent :
    - Volumes
    - Volumes are decoupled from container

---

# Intro To Docker Storage (cont.)

Non-persistent data:

- By default all container use local storage
- Storage locations:
    - Linux: /var/lib/docker/<storage-driver>
    - Window: c:\programdata\docker\windowsfilter\

---
# Intro To Docker Storage (cont.)

Storage drivers:

- RHEL family uses overlay2
- Ubuntu uses overlay2 or aufs
- Suse uses btrfs
- Windows has it's own special case...

---

# Intro To Docker Storage (cont.)

- Volumes:
    - Create the volume
    - Create your container
- Mount to a directory in the container
- Data is written to the volume
- Deleting a container does not delete the volume

---

# Intro To Docker Storage (cont.)

- First-class citizens
- Used the local driver as default
- 3rd party drivers
    - Block storage
    - File storage
    - Object storage

In any case of container loss, when the data is written to volume, it will be saved.

---

# Volume Commands

Start by getting some help with volume commands
```sh
 docker volume -h
```
List all the volumes
```sh
 docker volume ls
```
---

# Volume Commands (cont.)

Create new volume
```sh
 docker volume create my-storage
```
Inspect existing volumes
```sh
 docker volume inspect my-storage
```
---

# Volume Commands (cont.)

Remove unwanted volumes
```sh
 docker volume rm my-storage
```

Clean up dangling volumes
```sh
 docker volume prune
```
---

# Docker mount local directory

The `docker run` command first creates a writeable container layer over the specified image and then starts using the specified command.
Using the parameter -v allows you to bind a local directory.
`-v` or `--volume` allows you to mount local directories and files to your container. For example:

```sh
mkdir storage
docker run  -dit -v $(pwd)/storage:/internal_storage alpine /bin/sh
docker exec -it CONTAINER-ID /bin/sh -c `ls -l /`
```

---

# Practice

- Create local folder named html and in it create file called index.html
```sh
mkdir html; touch html/index.html
```
---

# Practice (cont.)

- Add to index.html 'hello from container' message
```sh
echo "Hello From Container" > html/index.html
```
---

# Practice (cont.)

- Run new instance of nginx container with dynamic port and try to access it
```sh
docker run -d -P nginx
docker ps # to get CONTAINER ID to test connection
docker inspect CONTAINER-ID # use output to get ip address
curl 127.0.0.1:dynamic-port # the dynamic port will be in the output of container inspect
```
---

# Practice (cont.)

- Run another instance of nginx container, yet this time mount to it the html folder created before hand to default path of nginx container located at `/usr/share/nginx/html`. Try to access it and see if there is a difference
```sh
docker run -d -P -v $(pwd)/html:/usr/share/nginx/html nginx # docker needs to 
docker ps # to get CONTAINER ID to test connection
docker inspect CONTAINER-ID # use output to get ip address
curl localhost:dynamic-port # to see what will be the output of container
```
---

# Practice (cont.)

- kill and remove all the running containers and remove all the image on your system
```sh
docker kill $(docker ps -q)
docker rm $(docker ps -a -q) # if `docker rm` will not work, use `-f` flag in addition to `-a` 
docker image rm $(docker image ls -q)
```

---

# Creating dedicated volumes for Storage

In addition to working with local directories,you can use Docker volumes. A Docker volume is a directory somewhere on your host machine and it can be mounted to one or many containers. They are fully managed and do not depend on certain operating system specifics that is enabled to by common file-systems  and can be used with different storage provider protocols like nfs, samba and so on. Lets see some examples:

```sh
docker volume ls
docker volume create my-storage
docker inspect my-storage
docker run -dit -P -v my-storage:/internal-storage alpine /bin/sh
docker exec -it CONTAINER-ID /bin/sh -c 'ls -la /' # here we're suppose to the the folder that mounts to storage
docker exec -it CONTAINER-ID /bin/sh -c 'touch /internal-storage/ninja-file'
docker kill CONTAINER-ID
# inspect the storage to see the ninja file
```

---

# Practice

- Create volume  net-volume
```sh
docker volume create net-volume
```
---

# Practice (cont.)

- Create container that uses volume net-volume
```sh
docker run -dit -v net-volume:/storage alpine /bin/sh
docker ps
docker exec -it CONTAINER-ID /bin/sh -c 'ls /storage'
```
---

# Practice (cont.)

- Access container save "foobar" file on the mounted volume
```sh
docker exec -it CONTAINER-ID /bin/sh 
touch  /storage/foobar
```
---

# Practice (cont.)

- Exit the container and kill it 
```sh
# if still in the container press [Ctrl+p] [Ctrl+q] on your keyboard
docker kill CONTAINER-ID
```
---

# Practice (cont.)

- Verify that foobar exists
```sh
docker inspect net-volume
ls PATH-TO-VOLUME
```
---

# Summary Practice

- Let’s create a Docker volume and mount it to persist MySQL data:
    - Create volume name mysql-data
    - Run mysql container in the background and mount it with storage
    - Kill mysql container
    - Verify that data still exists
    - Run new mysql container background and mount it with the same storage
    - Test that data is the same  and not corrupted (accessing remote folder should be enough)

