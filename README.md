# Docker Shallow Dive

### About The Course Itself
We'll learn several topics mainly focused on:
- What is docker ?
- Who needs docker ?
- How docker works ?
- How to manage docker in various environments


### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of containers
- For junior/senior developers who are still developing on LAMP/LEMP/WAMP/MAMP stacks
- Experienced ops who need refresher



### Course Topics

- Intro
- Setup
- Docker internals
- Docker basics
- Networking
- Storage
- Dockerfile
- Building and distributing images
- Docker Compose


> `[!]` Note: Please use build.sh script to create html version for your use


© All Right reserved to Alex M. Schapelle of VaioLabs ltd.
