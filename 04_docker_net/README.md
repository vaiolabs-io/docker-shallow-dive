
---
# 04 Docker Networking
<img src="../99_misc/.img/docker-network.png" alt="drawing" style="border-radius:25px;float:right;width:300px;">
<!-- ![bg right:40% contain](../99_misc/.img/docker-network.png) -->

---

# Intro To Docker Networking 

Socket networking:

- Container Network Model (CNM)
    - Essentially OSI model for containers
    - Designed to outlines fundamental building blocks of docker network
- The *libnetwork*
    - Real life implementation for CNM.
    - Used by docker to connect between containers.
    - Also used for service discovery, ingress (internal network) load balancing and network plane management functionality.

---

# Intro To Docker Networking (cont.)

### CNM Defines three building blocks:
<img src="../99_misc/.img/endpoint0.png" alt="endpoint" style="border-radius:25px;float:right;width:600px;">
<!-- ![bg right:40% contain](../99_misc/.img/endpoint0.png) -->

- `Sandboxes` : isolates network stack, meaning that containers do not have access to physical networking interfaces, routing tables, ports and dns.
- `Endpoints` : virtual network interfaces, responsible for connecting sandbox to a `network`
- `Networks` : software implementation of 802.1d bridge.
---

# Intro To Docker Networking (cont.)

### CNM Defines three building blocks:

As described in diagram, each container has `Sandbox` component which provides networking connectivity for containers. Container A has single `Endpoint` that connects the container A to *Network A*. In the mean while, Container B has 2 `Endpoints`, one connecting to *Network A* and other connecting to *Network B*. Container A and B can communicate with each other, because they are on the same *Network A*, however, in container B, 2 existing Endpoints can not communicate, unless there is some additional layer 3 routing involved.

--- 

# Networking Commands

List networks

```sh
 docker network ls
```

Getting detailed network information

```sh
 docker network inspect br01
```

Create new network

```sh
 docker network create br01
```

Delete  network

```sh
 docker network rm br01
```

---
# Networking Commands (cont.)


Connect a container to a user-defined bridge

```sh
 docker create --name my-nginx --network br02 nginx:latest
```

Connect pre-existing container to new network with:

`docker network connect <\NETWORK> <\CONTAINER>`

```sh
 docker network connect br02 my-nginx # assuming that my-nginx container was there before-hand
```
Disconnect a container from a user-defined bridge

```sh
 docker network disconnect br02 my-nginx
```

---

# Practice

- Start two alpine containers running shells with detached and interactive TTY's. provide them with 2 distinct names.
    - Connect to one of the alpine containers and try pinging :
        - Vaiolabs.io
        - Ip address of other container.
        - Host-name of other container.

---

# Practice (cont.)
```sh
docker run -dit alpine /bin/sh # -dit is a combination of -d and -it  together
docker run -dit alpine /bin/sh # this is the second container
docker ps # to get the container id
docker inspect FIRST-CONTAINER-ID # to get ip address
docker inspect SECOND-CONTAINER-ID
docker exec -it FIRST-CONTAINER-ID  /bin/sh
ping vaiolabs.io
ping SECOND-CONTAINER-ip-address
ping SECOND-CONTAINER-ID # usually used as host name and it is suppose to FAIL
```
---

# Practice (cont.)

- Create the alpine-net network with bridge driver.
    - Create two alpine containers that shall run on the alpine-net.
    - Create third alpine container that will NOT be part of alpine-net.
    - Try pinging alpine1 to alpine2 host-name from with in one of the containers.
    - Try ping IP address of alpine3 that is not in the same network.
    - Try pinging the host-name of alpine3.
    - Delete all containers and networks.

---

# Practice (cont.)

```sh
docker network ls
docker network create alpine-net
docker run -dit --network  alpine-net --name alp1 alpine /bin/sh # --name  is used to customize container name
docker run -dit --network  alpine-net --name alp2 alpine /bin/sh
docker run -dit  --name alp3 alpine /bin/sh # without -it ip will not be added
docker exec -it alp1 /bin/sh -c 'ping alp2-ip'
docker exec -it alp1 /bin/sh -c 'ping alp3-ip'
docker exec -it alp1 /bin/sh -c 'ping alp2' # using the --name we provided
docker exec -it alp1 /bin/sh -c 'ping alp3'
docker stop $(docker ps -q)
docker network rm alpine-net
```

<!-- ---

# Practice

- Create and start the nginx container as a detached process, yet also add `--rm` option.
    - Connect to it and check what ip does it has.
    - Validate that port 80 is accessible.
    - Try accessing from host the `localhost:80` -->

---


# Networking Containers

The networks that we are working on  can be also customized. Custom networks are usually required by microservice applications the require their own NAT segment that can me more agile in its structure. SCADAFence example.

Create a network with a subnet and gateway:

```sh
docker network create --subnet 10.100.0.0/16\
       --gateway 10.100.100.1 br02
```

```sh
docker network create --subnet 10.100.0.0/16\
      --gateway 10.100.100.1 --ip-range=10.100.4.0/24 br04
```

---

# Practice

- Create network with C-Segment ip 192.168.1.0/24 named docker-net
```sh
docker network create --subnet 192.168.1.0/24\
       --gateway 192.168.1.1  docker-net 
```
---

# Practice (cont.)

- Run alpine container that runs on docker-net
```sh
docker run -dit --network  docker-net\
    alpine /bin/sh  # we run shell because there is no process in container
```
---

# Practice (cont.)

- Validate that container main network is on docker-net
```sh
docker ps 
docker inspect CONTAINER-ID
```
---

# Practice (cont.)

- Kill the container
```sh
docker kill CONTAINER-ID
```
---

# Practice (cont.)

- Delete the custom network docker-net
```sh
docker network rm docker-net
```

---

# Networking  Ports

Although, not directly part of docker network, ports have significant place in grand scheme of things in docker networking. When running containers, we usually open connection to container, with exposing or publishing `port`.

- `--expose`: Expose a port or a range of ports
- `--publish` or `-p`/`-P` : Publish a container's port(s) to the host
    - `-P`: short for `--publish-all`, lets you publish all exposed ports to random ports on the host interfaces
    - `-p`: short for `--publish`,lets you publish a container’s specific port(s) to the Docker host

---

# Practice

- Create `ubuntu/apache2` container and expose port port 80 inside the container to a dynamic port
```sh
docker run -d  -P ubuntu/apache2
docker ps
```
---

# Practice (cont.)

- Create apache container exposed host port 8080 to container port 80
```sh
docker run -d -p 8080:80 ubuntu/apache2
```
---

# Practice (cont.)

- Create custom network named lamp-net
```sh
docker network create lamp-net
docker network ls
```
---

# Practice (cont.)

- Run `ubuntu/apache2` container on lamp-net network and publish port 8080
```sh
docker run -d --network lamp-net -p 8080:80 ubuntu/apache2
```
- There will be error due to port 8080 is already taken by different container,
-  so in order fix it, use different port number, e.g 8081

----

# Practice (cont.)

- Create mysql container that has external and internal ports of 3306 published and run on lamp-net
```sh
docker run -d --network lamp-net -p 3306:3306 mysql
docker ps -a # due to container not have connection to mysql container it might die immediately 
```
---

# Practice (cont.)

- Inspect mysql container and see the bind port for host

```sh
docker inspect CONTAINER-ID |grep 3306
```
---

# Practice (cont.)

- Stop all containers and remove them from docker access filesystem

```sh
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```
---

# Summary Practice

- Clean all containers and image from your host
- Create docker network named vaio-net
- Run alpine container on custom docker network named vaio-net
- Run nginx container on custom docker network named vaio-net
- Run ubuntu/apache2 container on default docker network
- Run from alpine container `ping` command to test connectivity with ubuntu/apache2
- BONUS: connect already running ubuntu/apache2 container to vaio-net
- Delete all the containers images and network
